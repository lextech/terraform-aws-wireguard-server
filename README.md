# README #

This is a terraform module to create a wireguard server.


```
module "wireguard" {
  source = "bitbucket.org/lextech/terraform-aws-wireguard-server"
  subnet_id = (id of a public subnet)
  s3_bucket = (name of an existing s3 bucket to store configuration)
}
```

### Optional parameters ###

* zone_id - zone id of a managed zone, will create a DNS record of {dns_name}.domain
* dns_name - prefix of dns record (default: "wg")
* key_pair_name - Key pair.  If given, allows SSH access to server to {ssh_ingress_cidr}
* ssh_ingress_cidr - CIDR block to allow SSH access to server (default: ["0.0.0.0/0"])
* name - name of resources (defaults to *vpc-name*_wireguard)
* instance_type - size of EC2 instance (default: "t3a.nano")
* ubuntu - version of ubuntu (default: "focal-20.04")
* peers - number of peer configurations to generate, must be greater than 1 (default: 1)
* internal_subnet_ip - Internal subnet ip (default: "10.13.13.0")
* dns - DNS server for wireguard clients (default: "auto")
* server_port - Port for wireguard server (default: 51820)
* timezone - Time zone for wireguard container (default: "America/Chicago")
* elastic_ip - Toggle creating an elastic IP address (default: true)
