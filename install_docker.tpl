#!/bin/bash
groupadd --system docker
usermod --append --groups docker ubuntu
snap install docker
apt-get update
apt-get install -y s3fs
sudo -i -u ubuntu mkdir /home/ubuntu/s3
echo "${s3_bucket} /home/ubuntu/s3 fuse.s3fs _netdev,allow_other,iam_role=auto 0 0" >> /etc/fstab
mount /home/ubuntu/s3
sudo -i -u ubuntu bash << EOF
cat <<EOT >> docker-compose.yaml
version: "2.1"
services:
  wireguard:
    image: linuxserver/wireguard
    container_name: wireguard
    cap_add:
      - NET_ADMIN
      - SYS_MODULE
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=${timezone}
      - SERVERURL=${server_url}
      - SERVERPORT=51820
      - PEERS=${peers}
      - PEERDNS=${dns}
      - INTERNAL_SUBNET=${internal_subnet}
    volumes:
      - /home/ubuntu/s3/${name}:/config
      - /lib/modules:/lib/modules
    ports:
      - ${server_port}:51820/udp
    sysctls:
      - net.ipv4.conf.all.src_valid_mark=1
    restart: unless-stopped

EOT
docker-compose up -d
EOF
