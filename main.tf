locals {
  name     = coalesce(var.name, "${replace(lower(data.aws_vpc.vpc.tags["Name"]), " ", "-")}_wireguard")
  dns_name = var.zone_id != null ? "${var.dns_name}.${data.aws_route53_zone.domain[0].name}" : null
  arch     = length(regexall("^t4g", var.instance_type)) > 0 ? "arm64" : "amd64"
}

data "aws_subnet" "subnet" {
  id = var.subnet_id
}

data "aws_vpc" "vpc" {
  id = data.aws_subnet.subnet.vpc_id
}

data "aws_route53_zone" "domain" {
  count   = var.zone_id != null ? 1 : 0
  zone_id = var.zone_id
}

# Find the latest ubuntu
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-${var.ubuntu}-${local.arch}-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "template_file" "install_docker" {
  template = file("${path.module}/install_docker.tpl")
  vars = {
    timezone        = var.timezone
    peers           = var.peers
    dns             = var.dns
    internal_subnet = var.internal_subnet_ip
    server_port     = var.server_port
    server_url      = var.zone_id != null ? local.dns_name : ""
    s3_bucket       = var.s3_bucket
    name            = local.name
  }
}

resource "aws_instance" "wireguard" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type
  key_name                    = var.key_pair_name
  vpc_security_group_ids      = [aws_security_group.wireguard.id]
  associate_public_ip_address = true
  subnet_id                   = var.subnet_id
  user_data                   = data.template_file.install_docker.rendered
  source_dest_check           = false
  iam_instance_profile        = aws_iam_instance_profile.ec2_profile.name

  tags = {
    Name = local.name
  }

  lifecycle {
    ignore_changes = [
      ami,
      user_data,
    ]
  }
}

resource "aws_security_group" "wireguard" {
  name        = local.name
  description = "Access to the wireguard server"
  vpc_id      = data.aws_vpc.vpc.id

  tags = {
    Name = local.name
  }
}

resource "aws_security_group_rule" "ssh" {
  count             = var.key_pair_name != null ? 1 : 0
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = var.ssh_ingress_cidr
  security_group_id = aws_security_group.wireguard.id
}


resource "aws_security_group_rule" "wireguard" {
  type              = "ingress"
  description       = "Access to wireguard"
  from_port         = var.server_port
  to_port           = var.server_port
  protocol          = "udp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.wireguard.id
}

resource "aws_security_group_rule" "outgoing" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.wireguard.id
}

resource "aws_route53_record" "server" {
  count   = var.zone_id != null ? 1 : 0
  zone_id = var.zone_id
  name    = local.dns_name
  type    = "A"
  ttl     = "300"
  records = var.elastic_ip ? [aws_eip.static[0].public_ip] : [aws_instance.wireguard.public_ip]
}

resource "aws_iam_role" "ec2" {
  name = "${local.name}-ec2-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "ec2_profile" {
  name = "${local.name}-ec2-profile"
  role = aws_iam_role.ec2.name
}

resource "aws_iam_role_policy" "ecs_s3" {
  name = "${local.name}-ec2-s3-policy"
  role = aws_iam_role.ec2.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::${var.s3_bucket}",
        "arn:aws:s3:::${var.s3_bucket}/*"
      ]
    }
  ]
}
EOF
}

resource "aws_eip" "static" {
  count    = var.elastic_ip ? 1 : 0
  instance = aws_instance.wireguard.id
  vpc      = true

  tags = {
    Name = local.name
  }
}
