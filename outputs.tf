output "dns_name" {
  value = local.dns_name != null ? local.dns_name : aws_instance.wireguard.public_dns
}

output "security_group" {
  value = aws_security_group.wireguard.id
}