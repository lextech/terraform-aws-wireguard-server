variable "subnet_id" {
  description = "Subnet to launch in EC2 in"
}

variable "s3_bucket" {
  description = "S3 bucket to store the Peer profiles in.  Will create /wireguard folder"
}

variable "zone_id" {
  description = "Zone ID for DNS record (optional)"
  default     = null
}

variable "key_pair_name" {
  description = "Key pair name for EC2 instance (optional)"
  default     = null
}

variable "ssh_ingress_cidr" {
  description = "List of CIDR block restrict SSH access to (optional)"
  default     = ["0.0.0.0/0"]
}

variable "name" {
  description = "Name of VPN (optional)"
  default     = null
}

variable "instance_type" {
  description = "Type of EC2 instance.  Default is t4g.nano, if not supported in region try t3a.nano (optional)"
  default     = "t4g.nano"
}

variable "ubuntu" {
  description = "Version of Ubuntu (optional)"
  default     = "focal-20.04"
}

variable "peers" {
  description = "Number of peer configurations to generate (optional)"
  default     = "1"
}

variable "internal_subnet_ip" {
  description = "Wireguard internal subnet (optional)"
  default     = "10.13.13.0"
}

variable "dns" {
  description = "DNS Server ip (optional)"
  default     = "auto"
}

variable "server_port" {
  description = "Port for wireguard server (optional)"
  default     = "51820"
}

variable "timezone" {
  description = "Time zone for wireguard container (optional)"
  default     = "America/Chicago"
}

variable "dns_name" {
  description = "Prefix for dns name (optional)"
  default     = "wg"
}

variable "elastic_ip" {
  description = "Create an elastic ip (optional)"
  default     = true
}
